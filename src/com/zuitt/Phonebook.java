package com.zuitt;

import java.util.ArrayList;

public class Phonebook {

    //encapsulation
    private ArrayList<Contact> contacts;

    public Phonebook() {
        contacts = new ArrayList<>();
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    //getter
    public ArrayList<Contact> getContact() {
        return contacts;
    }

    //setter
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    public void setContact(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void displayContact() {

        for (Contact contact : contacts) {

            System.out.println(contact.getName());
            System.out.println("-----------------------");
            System.out.println(contact.getName() + " has the following registered number: ");
            System.out.println(contact.getContactNumber());
            System.out.println(contact.getName() + " has the following registered address ");
            System.out.println(contact.getAddress());
            System.out.println();
        }
    }




}
