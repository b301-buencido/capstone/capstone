package com.zuitt;

public class Contact {

    // encapsulate
    private String name;
    private String contactNumber;
    private String address;

    //constructor

    public Contact () {
        this.name = "";
        this.contactNumber = "";
        this.address = "";
    }

    public Contact (String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //getter
    public String getName() {
        return name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getAddress() {
        return address;
    }




}
