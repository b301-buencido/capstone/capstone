package com.zuitt;


public class Main {

    public static void main (String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);


        if (phonebook.getContact().isEmpty()) {

            System.out.println("Phonebook is empty");
        }
        else {

            phonebook.displayContact();
        }

    }


}
